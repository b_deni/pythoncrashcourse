cities = {'warsaw': {'country': 'poland', 'population': '2 milions','fact': 'capital of Poland'},
            'new york': {'country': 'usa', 'population': '20 milions', "fact": "it's called big apple"},
            'budapest': {'country': 'hungary', 'population': '1 milion', 'fact': "don't know"},
          }

for city, info in cities.items():
    print(f"{city} is in {info['country']} and has population of {info['population']}")
    #print(f'{city} is in {cities['country']} and has population of {cities['population']}')