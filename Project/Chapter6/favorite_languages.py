favorite_languages = {
    'jen': ['python','ruby'],
    'sarah': ['c'],
    'edward': ['ruby', 'go'],
    'phil': ['python','haskell'],
}

#for name, language in favorite_languages.items():
#    print(f"{name.title()}'s favorite language is {language.title()}.")

#for language in set(favorite_languages.values()):
#    print(language.title())

for name, languages in favorite_languages.items():
    print(f"\n{name.title()}'s favorit languages are:")
    for language in languages:
        print(f"\t{language.title()}")