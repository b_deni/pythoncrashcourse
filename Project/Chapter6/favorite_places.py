favorite_places = {'Bart': ['at the computer', 'on the bike', 'in the car'], 'Donut': ['near the fridge', 'with tablet in hands', 'in the bathroom'], 'Jinx': ['in the car', 'on the race', 'with family']}
for name, places in favorite_places.items():
    print(f'{name} favorite places are:')
    for place in places:
        print(f'\t{place}')
