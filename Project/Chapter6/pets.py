cat = {'name': 'Owen', 'owner': 'Mark'}
dog = {'name': 'Jake', 'owner': 'Mark'}
fish = {'name': 'Fishee', 'owner': 'Zuzane'}

pets = [cat, dog, fish]

for pet in pets:
    print(pet['name'],pet['owner'])