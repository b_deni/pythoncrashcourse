favorite_languages = {
    'jen': 'python',
    'sarah': 'c',
    'edward': 'ruby',
    'phil': 'python',
}

should_vote = ['john','jen','arthur','mark']

for people in should_vote:
    if people in favorite_languages:
        print(f'Thank you {people} for taking a pool.')
    else:
        print(f'Hey {people} come, vote!')