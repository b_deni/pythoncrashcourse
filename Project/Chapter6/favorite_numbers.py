favnumbers = {'Bart': ['25','6'], 'Mark': ['13','7'], 'Rob': ['14']}
print(favnumbers['Bart'])
print(favnumbers.get('Bart'))

for name, numbers in favnumbers.items():
    print(f'{name} favorite numbers are: ')
    for number in numbers:
        print(number)
