#Program that prompts for the user's favorite number and store it in a file
import json

filename = 'fav_numbers.json'

favnumber = input("What's your favorite number?")
with open(filename,'w') as f:
    json.dump(favnumber,f)


with open(filename) as w:
    fav = json.load(w)
    print(f"Your favourite number is {fav}.")