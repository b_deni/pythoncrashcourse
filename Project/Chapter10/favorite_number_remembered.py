#Program that prompts for the user's favorite number and store it in a file
import json

filename = 'fav_numbers.json'

try:
    with open(filename) as f:
        fav = json.load(f)
except FileNotFoundError:
        favnumber = input("What's your favorite number?")
        with open(filename, 'w') as f:
            json.dump(favnumber, f)
else:
    print(f"Your favourite number is {fav}.")




