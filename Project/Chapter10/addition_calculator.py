
while True:
    try:
        number1 = input("Type first number\n")
        number1 = int(number1)
        number2 = input("Type second number\n")
        number2 = int(number2)
    except ValueError:
        print("Numbers only")
    else:
        result = int(number1) + int(number2)
        print(f'Sum of your numbers is  {result}.')