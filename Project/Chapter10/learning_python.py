"""Program reads a file and prints it three times : once the enitre file, once by looping over the file object, and once by
storing the lines in a list and then working with them outside the with block"""

filename = 'sample.txt'

with open(filename) as file_object:
    content = file_object.read()
print(content)
print('przerwa')
input()

with open(filename) as file_object2:
    lines = file_object2.readlines()
    for line in lines:
        print(line)

print('przerwa nr 2')
input()
with open(filename) as file_object3:
    blob = file_object3.readlines()

tablica = ''
for line in blob:
    tablica += line

print(tablica)