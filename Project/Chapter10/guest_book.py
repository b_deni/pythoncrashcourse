"""A while loop thath prompts user for their name and then print a greeting to the screen and save it to a file."""

filename = 'guest_book.txt'


while True:
    name = input("Hello, what's your name?")
    if name == 'quit':
        break
    else:
        with open(filename,'a') as file_object:
            file_object.write(name + '\n')
        print(f"Hi {name}, nice to meet you!")