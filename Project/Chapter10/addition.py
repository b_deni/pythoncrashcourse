"""Handle ValueError exception"""

number1 = input("Type first number\n")
number2 = input("Type second number\n")
try:
    result = int(number1) + int(number2)
except ValueError:
    print("You didn't type two numbers")
else:
    print(f'Sum of your numbers is  {result}.')