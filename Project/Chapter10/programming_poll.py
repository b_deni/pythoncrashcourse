"""Write people reasons why they like programming."""

filename = 'responses.txt'


while True:
    reason = input("Why do you like programming?")
    if reason == 'quit':
        break
    else:
        with open(filename,'a') as f:
            f.write(reason)
        print('Thanks for your answer')
