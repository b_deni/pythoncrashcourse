from random import randint

class Die:
    def __init__(self, sides=6):
        self.sides = sides

    def roll_die(self):
        print(randint(1,self.sides))

kostka6 = Die()
kostka6.roll_die()
kostka6.roll_die()
kostka6.roll_die()

kostka10 = Die(10)
kostka10.roll_die()
