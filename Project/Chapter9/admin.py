class User:
    """Class User with two attributes and two methods"""

    def __init__(self, first_name, last_name, age):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    def describe_user(self):
        print(f"User's information: {self.first_name}, {self.last_name}, {self.age}")

    def greet_user(self):
        print(f"Hello {self.first_name}!")


class Admin(User):
    """Simple attempt to Admin class inherited from User class."""

    def __init__(self, first_name, last_name, age):
        super().__init__(first_name, last_name, age)
        self.privileges = Privileges()


class Privileges:

    def __init__(self,privileges=[]):
        self.privileges = privileges

    def show_privileges(self):
        for privilege in self.privileges:
            print(privilege)



