class User:
    """Class User with two attributes and two methods"""

    def __init__(self, first_name, last_name, age):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.login_attempts = 0

    def describe_user(self):
        print(f"User's information: {self.first_name}, {self.last_name}, {self.age}")

    def greet_user(self):
        print(f"Hello {self.first_name}!")

    def increment_login_attempts(self, attempts):
        self.login_attempts += 1

    def reset_login_attempts(self):
        self.login_attempts = 0

ziomek = User('Bart', 'Simpson','35')
ziomek.describe_user()

ziomek2 = User('Mark', 'Twain', 'NA')
ziomek2.describe_user()
ziomek2.greet_user()

ziomek2.increment_login_attempts(1)
print(ziomek2.login_attempts)
ziomek2.increment_login_attempts(1)
print(ziomek2.login_attempts)
ziomek2.reset_login_attempts()
print(ziomek2.login_attempts)