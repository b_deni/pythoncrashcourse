class Restaurant:
    """Restaurant model with two methods and two attributes"""

    def __init__(self,restaurant_name, cuisine_type):
        self.restaurant_name = restaurant_name
        self.cuisine_type = cuisine_type

    def describe_restaurant(self):
        print(f"This is {self.restaurant_name} with {self.cuisine_type} cuisine.")

    def open_restaurant(self):
        print(f"The {self.restaurant_name} is open now.")

#restaurant = Restaurant('Viva Italia', 'french')
#print(restaurant.restaurant_name)
#restaurant.cuisine_type
#restaurant.describe_restaurant()
#restaurant.open_restaurant()