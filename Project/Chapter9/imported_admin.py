from Project.Chapter9.admin import Admin,User,Privileges

admin = Admin('Bart','Simpson',38)
admin.describe_user()

admin.privileges.privileges = ['can reset passwords']
admin.privileges.show_privileges()