import Project.Chapter9.car

my_beetle = Project.Chapter9.car.Car('volkswagen', 'beetle', 2019)
print(my_beetle.get_descriptive_name())

my_tesla = Project.Chapter9.car.ElectricCar('tesla', 'roadster', 2019)
print(my_tesla.get_descriptive_name())