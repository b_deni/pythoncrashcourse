class Restaurant:
    """Restaurant model with two methods and two attributes"""

    def __init__(self,restaurant_name, cuisine_type):
        self.restaurant_name = restaurant_name
        self.cuisine_type = cuisine_type
        self.number_served = 0

    def describe_restaurant(self):
        print(f"This is {self.restaurant_name} with {self.cuisine_type} cuisine with {self.number_served} customers served.")

    def open_restaurant(self):
        print(f"The {self.restaurant_name} is open now.")

    def set_number_served(self, number):
        self.number_served = number

    def increment_number_served(self, number):
        self.number_served += number

restaurant = Restaurant('Viva Italia', 'italian')
restaurant.describe_restaurant()
restaurant.number_served= 4
restaurant.describe_restaurant()
restaurant.set_number_served(10)
restaurant.describe_restaurant()
restaurant.increment_number_served(20)
restaurant.describe_restaurant()

#restaurant = Restaurant('Viva Italia', 'french')
#print(restaurant.restaurant_name)
#restaurant.cuisine_type
#restaurant.describe_restaurant()
#restaurant.open_restaurant()