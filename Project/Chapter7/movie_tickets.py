age = input("What's your age?: [or enter 'quit']")
active = True
while active:
    if age == 'quit':
        break
    age = int(age)
    print("You can buy a ticket for")

    if age < 3:
        print(" free.")
        active = False
    elif age > 3 and age <12:
        print(" 10$.")
        active = False
    elif age >12:
        print(" 15$.")
        active = False