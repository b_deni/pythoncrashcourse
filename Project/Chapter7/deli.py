sandwich_orders = ['cezar sandwich', 'regular sandwich', 'spicy sandwich']
finished_sandwiches = []

while sandwich_orders:
    current_sandwich = sandwich_orders.pop()
    print(f"I made you {current_sandwich}")
    finished_sandwiches.append(current_sandwich)

for finished_sandwiche in finished_sandwiches:
    print(finished_sandwiche)