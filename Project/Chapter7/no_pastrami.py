sandwich_orders = ['cezar sandwich','pastrami sandwich', 'pastrami sandwich', 'regular sandwich', 'spicy sandwich', 'pastrami sandwich']
finished_sandwiches = []

print("We run out of pastrami sandwiches")
while 'pastrami sandwich' in sandwich_orders:
    sandwich_orders.remove('pastrami sandwich')
while sandwich_orders:
    current_sandwich = sandwich_orders.pop()
    print(f"I made you {current_sandwich}")
    finished_sandwiches.append(current_sandwich)

for finished_sandwiche in finished_sandwiches:
    print(finished_sandwiche)