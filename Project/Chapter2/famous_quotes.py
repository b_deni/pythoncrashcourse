quote = "A goal without a plan is just a wish."
authors_name = "Antoine de Saint-Exupéry"
print(f'{authors_name} once said, "{quote}".')