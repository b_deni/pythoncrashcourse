import pizza
pizza.make_pizza(16, 'cheese')

from pizza import make_pizza
make_pizza(12, 'extra cheese')

from pizza import make_pizza as mp
mp(14, 'yoghurt')

import pizza as p
p.make_pizza(18, 'peppers')

from pizza import *
make_pizza(23, 'olives')