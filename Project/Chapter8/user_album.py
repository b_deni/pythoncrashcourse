def make_album(artist_name, artist_album,songs=None):
    description = {'name':artist_name, 'album':artist_album}
    if songs:
        description['songs']=songs
    return description

while True:
    print("Enter album name: ")
    print('Hit q to quit')
    album = input()
    if album == 'q':
        break
    print("Artist name: ")
    artist = input()
    if artist == 'q':
        break
    print(make_album(artist,album))

