def describe_city(name,country="Iceland"):
    print(f"{name} is in {country}.")

describe_city('Reykjavik')
describe_city('Paris','France')
