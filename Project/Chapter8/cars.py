def make_car(model,manufacturer,**car):
    car['model'] = model
    car['manufacturer'] = manufacturer
    return car

vehicle = make_car(manufacturer='subaru',model= 'outback', color='blue', tow_package=True)
print(vehicle)