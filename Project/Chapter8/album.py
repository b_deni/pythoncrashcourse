def make_album(artist_name, artist_album,songs=None):
    description = {'name':artist_name, 'album':artist_album}
    if songs:
        description['songs']=songs
    return description

bad = make_album('Michael Jackson','bad')
print(bad)
nirvana = make_album('Nirvana','Nirvana')
print(nirvana)
kowalska = make_album('Kasia Kowalska', 'Gemini',12)
print(kowalska)