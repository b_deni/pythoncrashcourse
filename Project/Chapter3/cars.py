cars = ['bmw', 'audi', 'toyota', 'subaru']
print('Original list:')
print(cars)
print('Reverse list:')
cars.sort(reverse=True)
print(cars)
print('Sorted:')
print(sorted(cars))

print('Cars list looks like this:')
print(cars)
print(len(cars))