def city_country(city, country, population=''):
    ccp1 = f"{city}, {country}"
    ccp1 = ccp1.title()
    if population == '':
        ccp = ccp1
    else:
        ccp2 = f" - population {population}"
        ccp = ccp1+ccp2
    return ccp

