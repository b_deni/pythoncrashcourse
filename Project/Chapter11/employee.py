class Employee:
    """Employee Class with first name, last name and a annual salary."""

    def __init__(self, first, last, salary):
        """Initialize the employee."""
        self.first = first
        self.last = last
        self.salary = salary

    def give_raise(self, ammount=5000):
        self.salary += ammount
