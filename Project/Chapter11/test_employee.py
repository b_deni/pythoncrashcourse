import unittest
from Project.Chapter11.employee import Employee


class TestEmployee(unittest.TestCase):
    """Test class for Employee class"""

    def setUp(self):
        """Create sample user with template data"""
        self.employee1 = Employee('Bart', 'Simpson', 34000)

    def test_give_default_raise(self):
        self.employee1.give_raise()
        self.assertEqual(self.employee1.salary, 39000)

    def test_give_custom_raise(self):
        self.employee1.give_raise(1000)
        self.assertEqual(self.employee1.salary, 35000)


if __name__ == '__main__':
    unittest.main()
