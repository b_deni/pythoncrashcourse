import unittest
from Project.Chapter11.city_functions import city_country


class CityCountryTest(unittest.TestCase):
    """Tests for 'city_function.py'."""

    def test_city_country(self):
        """Do City, Country words work."""
        formatted_citycountry = city_country('mielno', 'poland')
        self.assertEqual('Mielno, Poland', formatted_citycountry)

    def test_city_country_population(self):
        """Do City, Country, population works."""
        formatted_citycountrypopulation = city_country('mielno', 'poland', '10000')
        self.assertEqual("Mielno, Poland - population 10000", formatted_citycountrypopulation)


if __name__ == '__main__':
    unittest.main()
